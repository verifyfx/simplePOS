@echo off

set oldcp=%classpath%

REM ********************************************
REM *  Add Application Specific jar's here...  *
REM ********************************************

set classpath=JPOSApplication.jar
set classpath=%classpath%;lib/swing-layout-1.0.jar


REM ************************************
REM *  CITIZEN JavaPOS Specific jar's  *
REM ************************************

set classpath=%classpath%;CBMjpos.jar
set classpath=%classpath%;lib/comm.jar
set classpath=%classpath%;lib/jna.jar
set classpath=%classpath%;lib/xerces-2.6.0.jar
set classpath=%classpath%;lib/xerces2.jar
set classpath=%classpath%;.


java -cp %classpath% jposapplication.JPOSMain

set classpath=%oldcp%
