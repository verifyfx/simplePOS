import jpos.*;

public class Sample {
	public static void main(String[] args) {

		String PrinterDevice="CITIZEN S801 USB Windows";
		String DrawerDevice="CITIZEN S801 Cash Drawer 1 USB Windows";

		POSPrinter printer = new POSPrinter();
		CashDrawer drawer = new CashDrawer();

		try {

			printer.open(PrinterDevice);
			drawer.open(DrawerDevice);

			printer.claim(1000);
			drawer.claim(1000);

			printer.setDeviceEnabled(true);
			drawer.setDeviceEnabled(true);

			//Register BMP image
			printer.setBitmap(1, 2, "file:/C:/Sample01.bmp", POSPrinterConst.PTR_BM_ASIS, -1);

			//Start Transaction Print
			printer.transactionPrint(2, POSPrinterConst.PTR_TP_TRANSACTION);

			//Print the registered image
			printer.printNormal(2, "\033|cA\033|1B");

			printer.printNormal(2, "\n");

			printer.printNormal(2, "\033|cA800-421-6516\n");
			printer.printNormal(2, "\033|cATorrance, CA 90501\n");
			printer.printNormal(2, "\033|cAA RECEIPT IS REQUIRED FOR ALL RETURNS\n");

			printer.printNormal(2, "\n");

			printer.printNormal(2, "\033|rASALE  \n");

			printer.printNormal(2, "\n");

			//Items
			printer.printNormal(2, "  H MONTANA CONCERT BD                   20.00\n");
			printer.printNormal(2, "     786936767186\n");
			printer.printNormal(2, "     034469 BUNDLE AT $40       29.99-9.99\n");
			printer.printNormal(2, "  CAMP ROCK BD                           20.00\n");
			printer.printNormal(2, "     786936767391\n");
			printer.printNormal(2, "     034469 BUNDLE AT $40       29.99-9.99\n");

			printer.printNormal(2, "\n");

			printer.printNormal(2, "  VENDOR COUPON                          10.00-\n");
			printer.printNormal(2, "  SUBTOTAL                               30.00\n");
			printer.printNormal(2, "  TAX 8.250%                              3.30\n");
			printer.printNormal(2, "  \033|2CTotal            33.30\n");
			printer.printNormal(2, "  CASH                                   44.00\n");
			printer.printNormal(2, "  CHANGE DUE                             10.70\n");
			printer.printNormal(2, "  ITEM COUNT 2");

			printer.printNormal(2, "\n");

			printer.printNormal(2, "\033|cA\033|2CYOU SAVED $19.98\n");

			printer.printNormal(2, "\n\n\n");

			printer.printNormal(2, "\033|cAGet FREE product safety & recall info\n");
			printer.printNormal(2, "\033|cAwww.recalls.gov\n");
			printer.printNormal(2, "\033|cAFOR JOB OPPORTUNITIES, APPLY ONLINE\n");
			printer.printNormal(2, "\033|cAWWW.CSCAREERS.COM\n");

			printer.printNormal(2, "\n");

			printer.printNormal(2, "\033|cA08/19/08 11:30 AM\n");
			printer.printNormal(2, "\033|cAOPERATOR: TIM\n");

			printer.printNormal(2, "\n\n\n");

			printer.cutPaper(98);

			printer.printNormal(2, "\033|cA\033|3C10% off all strollers\n");
			printer.printNormal(2, "\033|cAExpires 09/19/08.\n");
			printer.printBarCode(2, "081908113008", POSPrinterConst.PTR_BCS_Code39, 100, 2, POSPrinterConst.PTR_BC_CENTER, POSPrinterConst.PTR_BC_TEXT_BELOW);

			printer.printNormal(2, "\n\n\n");

			printer.cutPaper(100);

			//Way to send ESC/POS sequence
//			printer.printNormal(2,"\033|2E\033\036");

			printer.transactionPrint(2, POSPrinterConst.PTR_TP_NORMAL);

			drawer.openDrawer();

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {

				printer.setDeviceEnabled(false);
				drawer.setDeviceEnabled(false);

				printer.release();
				drawer.release();

				printer.close();
				drawer.close();

			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.exit(0);
	}
}
