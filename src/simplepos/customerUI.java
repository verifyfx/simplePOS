package simplepos;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.table.*;

/**
 *
 * @author VerifyFX
 */
public class customerUI extends JFrame {
    //list of component here
    private JPanel defaultpane;
    private JLabel caption;
    private DefaultTableModel model;
    private JTable tblCustomer;
    private JButton btnDelItem, btnAddItem, btnReturnToMain;
    
    //list of variable here
    
    //main frame here (constructor)
    public customerUI() {
        setTitle("Manage Customer");
        setBounds(0,0,800,650); //x-pos, y-pos, width, height
        setResizable(false);
        setVisible(true);
        setLocationRelativeTo(null); //make it display in the center
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                exitting();
            }
        });
        
        defaultpane = (JPanel) getContentPane();
        defaultpane.setLayout(new BorderLayout());
        
        addCpmponents(); //initialization of frame is finished, adding component
    }

    private void addCpmponents() {
        //button creation
        btnDelItem = new JButton("Delete Selected Customer");
        btnAddItem = new JButton("Add Customer to List");
        btnReturnToMain = new JButton("Return to Main Menu");
        
        //label creation
        caption = new JLabel("Manage Customer in System");
        caption.setFont(new Font(null,1,18));
        
        //TABLE thing
        model = new DefaultTableModel();
        tblCustomer = new JTable(model);
        tblCustomer.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        model.addColumn("Customer Name");
        model.addColumn("Customer Address");
        for(int i=0 ; i<mainUI.myCustomer.size();i++) {
            model.addRow(new Object[]{mainUI.myCustomer.get(i).getName(), mainUI.myCustomer.get(i).getAddress()});
        }
        
        //add Commands
        btnDelItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[] options = {"Yes", "No"};
                int ch = JOptionPane.showOptionDialog(defaultpane,"Do you want to remove selected customer?", "simplePOS", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[1]);
                if(ch==0) {
                    int rw = tblCustomer.getSelectedRow();
                    if(rw!=-1) {
                        model.removeRow(rw);
                        mainUI.myCustomer.remove(rw);
                        mainUI.hasCustomerChg = true;
                    } else {
                        JOptionPane.showMessageDialog(defaultpane, "No row selected", "Error", JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        });
        btnAddItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int initSize = mainUI.myCustomer.size();
                String rName = null;
                String rAddr = null;
                rName = JOptionPane.showInputDialog(null, "Enter Customer Name : ", "Add Customer", JOptionPane.QUESTION_MESSAGE);
                rAddr = JOptionPane.showInputDialog(null, "Enter Customer Address : ", "Add Customer", JOptionPane.QUESTION_MESSAGE);
                
                mainUI.myCustomer.add(new Customer(rName,rAddr));
                
                if(initSize+1 == mainUI.myCustomer.size()) {
                    model.addRow(new Object[]{mainUI.myCustomer.get(initSize).getName(), mainUI.myCustomer.get(initSize).getAddress()});
                    mainUI.hasCustomerChg = true;
                }
            }
        });
        btnReturnToMain.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exitting();
            }
        });
        
        //set things in place
        JPanel header = new JPanel(new BorderLayout());
        header.add(caption,BorderLayout.WEST);
        JPanel control = new JPanel(new GridLayout(3,1,4,4));
        control.add(btnReturnToMain);
        control.add(btnAddItem);
        control.add(btnDelItem);
        header.add(control,BorderLayout.EAST);
        JScrollPane body = new JScrollPane(tblCustomer);
        tblCustomer.setFillsViewportHeight(true);
        
        defaultpane.add(header,BorderLayout.PAGE_START);
        defaultpane.add(body,BorderLayout.CENTER);
        validate();
    }
    
    public void exitting() {
        Object[] options = {"Yes", "No"};
        int ch = JOptionPane.showOptionDialog(defaultpane,"Do you want to exit", "simplePOS", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        if(ch==0) {
            this.dispose();
        }
    }
    
    public boolean isUniquePID(int input) {
        boolean unique = true;
        for(int i = 0 ; i < mainUI.myProduct.size(); i++) {
            if(input == mainUI.myProduct.get(i).getProdID()) {
                unique = false;
                break;
            }
        }
        return unique;
    }
}
