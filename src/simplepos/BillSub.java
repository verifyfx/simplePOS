/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simplepos;

/**
 *
 * @author VerifyFX
 */
public class BillSub {
    private String prodName;
    private int qty;
    private double price;
    private double total;

    public BillSub(String prodName, int qty, double price) {
        this.prodName = prodName;
        this.qty = qty;
        this.price = price;
        this.total = (double) qty * price;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return total;
    }
    
    
}
