package simplepos;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.table.*;

/**
 *
 * @author VerifyFX
 */
public class productUI extends JFrame {
    //list of component here
    private JPanel defaultpane;
    private JLabel caption;
    private DefaultTableModel model;
    private JTable tblProduct;
    private JButton btnDelItem, btnAddItem, btnEditItem, btnReturnToMain;
    
    //list of variable here
    
    //main frame here (constructor)
    public productUI() {
        setTitle("Manage Product");
        setBounds(0,0,800,650); //x-pos, y-pos, width, height
        setResizable(false);
        setVisible(true);
        setLocationRelativeTo(null); //make it display in the center
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                exitting();
            }
        });
        
        defaultpane = (JPanel) getContentPane();
        defaultpane.setLayout(new BorderLayout());
        
        addCpmponents(); //initialization of frame is finished, adding component
    }

    private void addCpmponents() {
        //button creation
        btnDelItem = new JButton("Delete Selected Item");
        btnAddItem = new JButton("Add Item to List");
        //btnEditItem = new JButton("Edit Selected Item");
        btnReturnToMain = new JButton("Return to Main Menu");
        
        //label creation
        caption = new JLabel("Manage Product in System");
        caption.setFont(new Font(null,1,18));
        
        //TABLE thing
        model = new DefaultTableModel();
        tblProduct = new JTable(model);
        tblProduct.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        model.addColumn("Product ID");
        model.addColumn("Product Name");
        model.addColumn("Price");
        model.addColumn("Barcode");
        for(int i=0 ; i<mainUI.myProduct.size();i++) {
            model.addRow(new Object[]{mainUI.myProduct.get(i).getProdID(), mainUI.myProduct.get(i).getProdName(), mainUI.myProduct.get(i).getPrice(), mainUI.myProduct.get(i).getBarcode()});
        }
        
        //add Commands
        btnDelItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[] options = {"Yes", "No"};
                int ch = JOptionPane.showOptionDialog(defaultpane,"Do you want to remove?", "simplePOS", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[1]);
                if(ch==0) {
                    int rw = tblProduct.getSelectedRow();
                    if(rw!=-1) {
                        model.removeRow(rw);
                        mainUI.myProduct.remove(rw);
                        mainUI.hasProductChg = true;
                    } else {
                        JOptionPane.showMessageDialog(defaultpane, "No row selected", "Error", JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        });
        btnAddItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int initSize = mainUI.myProduct.size();
                String rPID = null;
                String rName = null;
                String rPrice = null;
                do {
                    rPID = JOptionPane.showInputDialog(null, "Enter Product ID : ", "Add Product", JOptionPane.QUESTION_MESSAGE);
                } while(!isUniquePID(Integer.parseInt(rPID)));
                rName = JOptionPane.showInputDialog(null, "Enter Product Name : ", "Add Product", JOptionPane.QUESTION_MESSAGE);
                rPrice = JOptionPane.showInputDialog(null, "Enter Product Price : ", "Add Product", JOptionPane.QUESTION_MESSAGE);
                
                mainUI.myProduct.add(new Product(Integer.parseInt(rPID),rName,Double.parseDouble(rPrice)));
                
                if(initSize+1 == mainUI.myProduct.size()) {
                    model.addRow(new Object[]{mainUI.myProduct.get(initSize).getProdID(), mainUI.myProduct.get(initSize).getProdName(), mainUI.myProduct.get(initSize).getPrice(), mainUI.myProduct.get(initSize).getBarcode()});
                    mainUI.hasCustomerChg = true;
                }
            }
        });
        
        /*btnEditItem.addMouseListener(new MouseListener () {
            @Override
            public void mouseClicked(MouseEvent me) {
                int rw = tblProduct.getSelectedRow();
                if(rw!=-1) {
                    //some code to edit thing
                    mainUI.hasProductChg = true;
                } else {
                    JOptionPane.showMessageDialog(defaultpane, "No row selected", "Error", JOptionPane.WARNING_MESSAGE);
                }
            }

            @Override
            public void mousePressed(MouseEvent me) {}

            @Override
            public void mouseReleased(MouseEvent me) {}

            @Override
            public void mouseEntered(MouseEvent me) {}

            @Override
            public void mouseExited(MouseEvent me) {}
        
        });*/
        btnReturnToMain.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exitting();
            }
        });
        
        //set things in place
        JPanel header = new JPanel(new BorderLayout());
        header.add(caption,BorderLayout.WEST);
        JPanel control = new JPanel(new GridLayout(3,1,4,4));
        control.add(btnReturnToMain);
        control.add(btnAddItem);
        control.add(btnDelItem);
        //control.add(btnEditItem);
        header.add(control,BorderLayout.EAST);
        JScrollPane body = new JScrollPane(tblProduct);
        tblProduct.setFillsViewportHeight(true);
        
        defaultpane.add(header,BorderLayout.PAGE_START);
        defaultpane.add(body,BorderLayout.CENTER);
        validate();
    }
    
    public void exitting() {
        Object[] options = {"Yes", "No"};
        int ch = JOptionPane.showOptionDialog(defaultpane,"Do you want to exit", "simplePOS", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        if(ch==0) {
            this.dispose();
        }
    }
    
    public boolean isUniquePID(int input) {
        boolean unique = true;
        for(int i = 0 ; i < mainUI.myProduct.size(); i++) {
            if(input == mainUI.myProduct.get(i).getProdID()) {
                unique = false;
                break;
            }
        }
        return unique;
    }
}
