package simplepos;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author VerifyFX
 */
public class Product {

    private int prodID;
    private String prodName;
    private double price;
    private String barcode;

    public Product(int prodID, String prodName, double price) {
        this.prodID = prodID;
        this.prodName = prodName;
        this.price = price;
        this.barcode = normalizeBarcode(Integer.toString(prodID));
        this.barcode += Integer.toString(checkSum(this.barcode));
    }

    public int getProdID() {
        return prodID;
    }

    public void setProdID(int prodID) {
        this.prodID = prodID;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getBarcode() {
        return barcode;
    }

    private String normalizeBarcode(String input) {
        if (input.length() < 7) {
            int needChar = 7 - input.length();
            String zChar = "";
            for (int i = 0; i < needChar; i++) {
                zChar += "0";
            }
            input = zChar+input;
        }
        return input;
    }

    public int checkSum(String in) {
        char[] code = in.toCharArray();
        int sum1 = code[1] + code[3] + code[5];
        int sum2 = 3 * (code[0] + code[2] + code[4] + code[6]);

        int checksum_value = sum1 + sum2;
        int checksum_digit = 10 - (checksum_value % 10);
        if (checksum_digit == 10) checksum_digit = 0;
        return checksum_digit;
    }
}
