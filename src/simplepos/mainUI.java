package simplepos;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.text.SimpleDateFormat;

/**
 *
 * @author VerifyFX
 */
public class mainUI extends JFrame {
    //list of component here
    private JPanel defaultpane;
    private JButton btnMakeBill, btnMakeBarcode, btnEditCustomer, btnEditProduct, btnSetting, btnExit;
    private JLabel lblLogo, lblCompanyName, lblVersion;
    private MyImageIcon imgLogo;
    
    //list of variable here
    final String VERSION = "0.1a";
    public static Company myCompany;
    public static ArrayList<Product> myProduct = new ArrayList<>();
    public static ArrayList<Customer> myCustomer = new ArrayList<>();
    public static ArrayList<Bill> myBill = new ArrayList<>();
    public static boolean hasCompanyChg = false;
    public static boolean hasProductChg = false;
    public static boolean hasCustomerChg = false;
    
    
    //main frame here (constructor)
    public mainUI() {
        loadResource();
        setTitle("simplePOS - "+myCompany.getCompanyName());
        setBounds(0,0,420,550); //x-pos, y-pos, width, height
        setResizable(false);
        setVisible(true);
        setLocationRelativeTo(null); //make it display in the center
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                exitting();
            }
        });
        
        defaultpane = (JPanel) getContentPane();
        defaultpane.setLayout(new BorderLayout());
        
        addCpmponents(); //initialization of frame is finished, adding component
    }

    private void addCpmponents() {
        //button creation
        btnMakeBill = new JButton("Create new Bill");
        btnMakeBarcode = new JButton("Create Barcode Label");
        btnEditCustomer = new JButton("Edit Customer List");
        btnEditProduct = new JButton("Edit Product List");
        btnSetting = new JButton("Program Settings");
        btnExit = new JButton("Exit Program");
        //label creation
        imgLogo = new MyImageIcon("res/cashRegisterLogo.png").resize(80, 80);
        lblLogo = new JLabel(imgLogo);
        lblCompanyName = new JLabel(myCompany.getCompanyName());
        lblCompanyName.setFont(new Font(null,1,14));
        lblVersion = new JLabel("Program Version: "+VERSION);
        
        //add commands
        btnMakeBill.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new billUI();
            }
        });
        btnMakeBarcode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new barcodeUI();
            }
        });
        btnEditCustomer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new customerUI();
            }
        });
        btnEditProduct.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new productUI();
            }
        });
        btnSetting.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new settingUI();
            }
        });
        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exitting();
            }
        });
        
        //set things in place
        JPanel header = new JPanel();
        header.setBounds(0, 0, 420, 100);
        header.add(lblLogo);
        header.add(lblCompanyName);
        JPanel control = new JPanel();
        control.setLayout(new GridLayout(5,1,4,4));
        control.add(btnMakeBill);
        //control.add(btnMakeBarcode);
        control.add(btnEditCustomer);
        control.add(btnEditProduct);
        control.add(btnSetting);
        control.add(btnExit);
        JPanel footer = new JPanel();
        footer.add(lblVersion);
        defaultpane.add(header, BorderLayout.PAGE_START);
        defaultpane.add(control, BorderLayout.CENTER);
        defaultpane.add(footer, BorderLayout.PAGE_END);
        
        pack();
        validate();
        
    }
    
    public void exitting() {
        Object[] options = {"Yes", "No"};
        int ch = JOptionPane.showOptionDialog(defaultpane,"Do you want to exit", "simplePOS", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        if(ch==0) {
            writeResource();
            System.exit(0);
        }
    }
    
    private static void loadResource() {
        Scanner fs = null;
        //load company info
        try {
            fs = new Scanner(new File("company.csv"));
            String[] tmp = fs.nextLine().split("\\s*,\\s*");
            myCompany = new Company(tmp[0],tmp[1],tmp[2],tmp[3],tmp[4],tmp[5]);
        } catch (FileNotFoundException e) {
            System.out.println("Error opening file company, file not found.\r\nExiting...");
            System.exit(0);
        } catch (Exception e) {
            System.out.println("Error in reading file company.\r\nExiting...");
            System.exit(0);
        }
        fs.close();
        //load product info
        try {
            fs = new Scanner(new File("product.csv"));
            while(fs.hasNextLine()) {
                String[] tmp = fs.nextLine().split("\\s*,\\s*");
                myProduct.add(new Product(Integer.parseInt(tmp[0]),tmp[1],Double.parseDouble(tmp[2])));
            }
        } catch (FileNotFoundException e) {
            System.out.println("Error opening file product, file not found.\r\nExiting...");
            System.exit(0);
        } catch (Exception e) {
            System.out.println("Error in reading file product.\r\nExiting...");
            e.printStackTrace();
            System.exit(0);
        }
        fs.close();
        //load customer info
        try {
            fs = new Scanner(new File("customer.csv"));
            while(fs.hasNextLine()) {
                String[] tmp = fs.nextLine().split("\\s*,\\s*");
                myCustomer.add(new Customer(tmp[0],tmp[1]));
            }
        } catch (FileNotFoundException e) {
            System.out.println("Error opening file product, file not found.\r\nExiting...");
            System.exit(0);
        } catch (Exception e) {
            System.out.println("Error in reading file product.\r\nExiting...");
            e.printStackTrace();
            System.exit(0);
        }
        fs.close();
    }
    
    private static void writeResource(){
        PrintWriter pw = null;
        //write information back
        if(hasCompanyChg) {
            try {
                pw = new PrintWriter(new FileOutputStream("company.csv",false));
            } catch (Exception e) {
                System.out.println("Can't open company.csv for saving... changes will not be made");
            }
            pw.printf("%s,%s,%s,%s,%s,%s", myCompany.getCompanyName(),myCompany.getAddress1(),myCompany.getAddress2(),myCompany.getAddress3(),myCompany.getTelephone1(),myCompany.getTelephone2());
            pw.close();
        }
        
        if(hasProductChg) {
            try {
                pw = new PrintWriter(new FileOutputStream("product.csv",false));
            } catch (Exception e) {
                System.out.println("Can't open product.csv for saving... changes will not be made");
            }
            for(int i = 0 ; i<myProduct.size();i++) {
                pw.printf("%d,%s,%.2f",myProduct.get(i).getProdID(),myProduct.get(i).getProdName(),myProduct.get(i).getPrice());
                if(i!=myProduct.size()-1) {
                    pw.printf("\r\n");
                }
            }
            pw.close();
        }
        
        if(hasCustomerChg) {
            try {
                pw = new PrintWriter(new FileOutputStream("customer.csv",false));
            } catch (Exception e) {
                System.out.println("Can't open customer.csv for saving... changes will not be made");
            }
            for(int i = 0 ; i<myCustomer.size();i++) {
                pw.printf("%s,%s",myCustomer.get(i).getName(),myCustomer.get(i).getAddress());
                if(i!=myCustomer.size()-1) {
                    pw.printf("\r\n");
                }
            }
            pw.close();
        }
        //exportBill();
    }
    
    /*private static void exportBill() {
        PrintWriter pw = null;
        try{
            String time = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
            pw = new PrintWriter(new FileOutputStream("billing"+time+".csv",true));
            for(Bill b: myBill) {
                pw.printf("Time of transaction %s\r\n", b.getTimestamp());
                pw.printf("Customer: %s\r\n", b.getCustName());
                pw.printf("Address : %s\r\n\r\n", b.getCustAddr());
                pw.printf("ID,Product Name,Quantity,Price,Total\r\n");
                for(int i=0; i<b.getItem().size(); i++) {
                    pw.printf("%d,%s,%d,%.2f,%.2f\r\n", i+1,b.getItem().get(i).getProdName(),b.getItem().get(i).getQty(),b.getItem().get(i).getPrice(),b.getItem().get(i).getTotal());
                }
                pw.printf("\r\n Subtotal, %.2f\r\n", b.getSubTotal());
                pw.printf("Discount, %.2f\r\n", b.getDiscount());
                pw.printf("Total, %.2f\r\n\r\n\r\n", b.getTotal());
            }
        } catch (Exception e) {
            System.out.printf("Something went wrong in writing report");
        }
        pw.close();
    }*/
}
