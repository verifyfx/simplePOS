package simplepos;

/**
 *
 * @author VerifyFX
 */
public class Customer {
    private String name;
    private String address;

    public Customer(String Name, String address) {
        this.name = Name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String Name) {
        this.name = Name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
}
