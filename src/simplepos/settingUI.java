/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simplepos;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author VerifyFX
 */
public class settingUI extends JFrame{
    //list of component here
    private JPanel defaultpane;
    private JLabel lblCompanyName, lblAddr1, lblAddr2, lblAddr3, lblPhone1, lblPhone2;
    private JTextField txtCompanyName, txtAddr1, txtAddr2, txtAddr3, txtPhone1, txtPhone2;
    private JButton btnSave, btnCancel;
    
    //list of variable here
    
    //main frame here (constructor)
    public settingUI() {
        setTitle("Settings");
        setBounds(0,0,500,500); //x-pos, y-pos, width, height
        setResizable(false);
        setVisible(true);
        setLocationRelativeTo(null); //make it display in the center
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                exitting();
            }
        });
        
        defaultpane = (JPanel) getContentPane();
        defaultpane.setLayout(null);
        
        addCpmponents(); //initialization of frame is finished, adding component
    }

    private void addCpmponents() {
        //label creation
        lblCompanyName = new JLabel("Company Name :", JLabel.TRAILING);
        lblAddr1 = new JLabel("Address line 1 :", JLabel.TRAILING);
        lblAddr2 = new JLabel("Address line 2 :", JLabel.TRAILING);
        lblAddr3 = new JLabel("Address line 3 :", JLabel.TRAILING);
        lblPhone1 = new JLabel("Phone No 1 :", JLabel.TRAILING);
        lblPhone2 = new JLabel("Phone No 2 :", JLabel.TRAILING);
        //textfield creation
        txtCompanyName = new JTextField(mainUI.myCompany.getCompanyName());
        txtAddr1 = new JTextField(mainUI.myCompany.getAddress1());
        txtAddr2 = new JTextField(mainUI.myCompany.getAddress2());
        txtAddr3 = new JTextField(mainUI.myCompany.getAddress3());
        txtPhone1 = new JTextField(mainUI.myCompany.getTelephone1());
        txtPhone2 = new JTextField(mainUI.myCompany.getTelephone2());
        
        //button creation
        btnSave = new JButton("Save");
        btnCancel = new JButton("Cancel");
        //add Commands
        btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[] options = {"Yes", "No"};
                int ch = JOptionPane.showOptionDialog(defaultpane,"Do you want to save these information?", "simplePOS", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
                if(ch==0) {
                    mainUI.myCompany.setCompanyName(txtCompanyName.getText());
                    mainUI.myCompany.setAddress1(txtAddr1.getText());
                    mainUI.myCompany.setAddress2(txtAddr2.getText());
                    mainUI.myCompany.setAddress3(txtAddr3.getText());
                    mainUI.myCompany.setTelephone1(txtPhone1.getText());
                    mainUI.myCompany.setTelephone2(txtPhone2.getText());
                    JOptionPane.showMessageDialog(defaultpane, "Save complete", "simplePOS", 1);
                    mainUI.hasCompanyChg = true;
                    exitting();
                }
            }
        });
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exitting();
            }
        });
        
        //set things in place
        JPanel form = new JPanel(new SpringLayout());
        form.setBounds(0, 10, 500, 390);
        form.add(lblCompanyName);
        lblCompanyName.setLabelFor(txtCompanyName);
        form.add(txtCompanyName);
        form.add(lblAddr1);
        lblAddr1.setLabelFor(txtAddr1);
        form.add(txtAddr1);
        form.add(lblAddr2);
        lblAddr2.setLabelFor(txtAddr2);
        form.add(txtAddr2);
        form.add(lblAddr3);
        lblAddr3.setLabelFor(txtAddr3);
        form.add(txtAddr3);
        form.add(lblPhone1);
        lblPhone1.setLabelFor(txtPhone1);
        form.add(txtPhone1);
        form.add(lblPhone2);
        lblPhone2.setLabelFor(txtPhone2);
        form.add(txtPhone2);
        SpringUtilities.makeCompactGrid(form, 6, 2, 3, 3, 3, 3);
        defaultpane.add(form);
        JPanel control = new JPanel(new FlowLayout(FlowLayout.CENTER));
        control.setBounds(0,410,500,90);
        control.add(btnSave);
        control.add(btnCancel);
        defaultpane.add(control);
        validate();
    }
    
    public void exitting() {
        Object[] options = {"Yes", "No"};
        int ch = JOptionPane.showOptionDialog(defaultpane,"Do you want to exit", "simplePOS", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        if(ch==0) {
            this.dispose();
        }
    }
}
