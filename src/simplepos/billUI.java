package simplepos;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.KeyStroke;
import java.awt.print.*;
import javax.print.PrintService;

/**
 *
 * @author VerifyFX
 */
public class billUI extends JFrame implements Printable {
    //list of component here
    private JPanel defaultpane;
    private JLabel lblCaption, lblCustName, lblCustAddr, lblSubTotal, lblDiscount, lblTotal;
    private JComboBox txtCustName;
    private JTextField txtCustAddr, txtSubTotal, txtDiscount, txtTotal;
    private JButton btnClear, btnSave, btnPrintSave, btnReturnMain, btnAddItem, btnRemoveItem;
    private JTable tblBilling;
    private DefaultTableModel model;
    private JRadioButton[] radPacking;
    private ButtonGroup grpPack;

    
    //list of variable here
    private ArrayList<BillSub> currentBill;
    private String packMode = "Plastic Bag";
    private double payRec = 0;
    
    //main frame here (constructor)
    public billUI() {
        setTitle("");
        setBounds(0,0,800,600); //x-pos, y-pos, width, height
        setResizable(false);
        //setExtendedState(JFrame.MAXIMIZED_BOTH);
        setVisible(true);
        setLocationRelativeTo(null); //make it display in the center
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                exitting();
            }
        });
        
        defaultpane = (JPanel) getContentPane();
        defaultpane.setLayout(new BoxLayout(defaultpane, BoxLayout.Y_AXIS));
        
        addCpmponents(); //initialization of frame is finished, adding component
        initBill();
    }

    private void addCpmponents() {
        //button creation
        btnClear = new JButton("Cancel This Bill");
        btnSave = new JButton("Save Only");
        btnPrintSave = new JButton("Print");
        btnReturnMain = new JButton("Return to Main");
        btnAddItem = new JButton("Add Item to Cart - F5");
        btnRemoveItem = new JButton("Remove Selected Item from Cart - F6");
        
        //label creation + text field
        lblCaption = new JLabel("Billing");
        lblCaption.setFont(new Font(null,1,20));
        lblCustName = new JLabel("Customer Name");
        txtCustName = new JComboBox();
        lblCustAddr = new JLabel("Customer Address");
        txtCustAddr = new JTextField("");
        lblSubTotal = new JLabel("Sub Total");
        txtSubTotal = new JTextField("0.00");
        txtSubTotal.setEnabled(false);
        txtSubTotal.setHorizontalAlignment(SwingConstants.RIGHT);
        txtSubTotal.setDisabledTextColor(Color.BLACK);
        lblDiscount = new JLabel("Discount");
        txtDiscount = new JTextField("0.00");
        txtDiscount.setHorizontalAlignment(SwingConstants.RIGHT);
        lblTotal = new JLabel("Total");
        lblTotal.setFont(new Font(null,1,18));
        txtTotal = new JTextField("0.00");
        txtTotal.setFont(new Font(null,1,18));
        txtTotal.setEnabled(false);
        txtTotal.setHorizontalAlignment(SwingConstants.RIGHT);
        txtTotal.setDisabledTextColor(Color.BLACK);
        
        JLabel smallB = new JLabel("Baht");
        JLabel bigB = new JLabel("Baht");
        bigB.setFont(new Font(null,1,18));
        
        //Radiobutton
        ActionListener ax = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                packMode = e.getActionCommand();
            }
        };
        grpPack = new ButtonGroup();
        radPacking = new JRadioButton[5];
        radPacking[0] = new JRadioButton("Plastic Bag");
        radPacking[0].setName("Plastic Bag");
        radPacking[0].setActionCommand("Plastic Bag");
        radPacking[0].addActionListener(ax);
        radPacking[0].setSelected(true);
        radPacking[1] = new JRadioButton("Gift Warping");
        radPacking[1].setName("Gift Warping");
        radPacking[1].setActionCommand("Gift Warping");
        radPacking[1].addActionListener(ax);
        radPacking[2] = new JRadioButton("Bubble Warping");
        radPacking[2].setName("Bubble Warping");
        radPacking[2].setActionCommand("Bubble Warping");
        radPacking[2].addActionListener(ax);
        radPacking[3] = new JRadioButton("Delivery");
        radPacking[3].setName("Delivery");
        radPacking[3].setActionCommand("Delivery");
        radPacking[3].addActionListener(ax);
        radPacking[4] = new JRadioButton("No Packing");
        radPacking[4].setName("No Packing");
        radPacking[4].setActionCommand("No Packing");
        radPacking[4].addActionListener(ax);
        for(int i = 0 ; i<5 ;i++) {
            grpPack.add(radPacking[i]);
        }
        
        //table thingy
        model = new DefaultTableModel();
        tblBilling = new JTable(model);
        tblBilling.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        model.addColumn("No.");
        model.addColumn("Product Name");
        model.addColumn("Qty.");
        model.addColumn("Price");
        model.addColumn("Total");
        
        //add Commands
        for(Customer c : mainUI.myCustomer) {
            txtCustName.addItem(c.getName());
        }
        updateCustAddr();
        txtCustName.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                updateCustAddr();
            }
            
        });
        btnReturnMain.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exitting();
            }
        });
        Action aa = new AbstractAction("Add Item to Cart - F5") {
            @Override
            public void actionPerformed(ActionEvent e) {
                addItem();
            }
        };
        btnAddItem.setAction(aa);
        btnAddItem.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F5,0),btnAddItem.getActionCommand());
        btnAddItem.getActionMap().put(btnAddItem.getActionCommand(), aa);
        Action bb = new AbstractAction("Remove Selected Item from Cart - F6") {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeItem();
            }
        };
        btnRemoveItem.setAction(bb);
        btnRemoveItem.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F6,0),btnRemoveItem.getActionCommand());
        btnRemoveItem.getActionMap().put(btnRemoveItem.getActionCommand(), bb);
        btnClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[] options = {"Yes", "No"};
                int ch = JOptionPane.showOptionDialog(defaultpane,"Do you want to clear this bill?", "simplePOS", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
                if(ch==0) {
                    initBill();
                }
            }
        });
        Action cc = new AbstractAction("Print - F9") {
            @Override
            public void actionPerformed(ActionEvent e) {
                double dPay = 0;
                while(dPay<Double.parseDouble(txtTotal.getText())) {
                    String pay = JOptionPane.showInputDialog(defaultpane,"Total: "+txtTotal.getText()+" Please get payment","simplePOS",JOptionPane.QUESTION_MESSAGE);
                    try {
                        dPay = Double.parseDouble(pay);
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(defaultpane, "Error getting value, try again");
                        dPay = 0;
                    }
                    if(dPay>=Double.parseDouble(txtTotal.getText())) break;
                    JOptionPane.showMessageDialog(defaultpane, "Payment is too less");
                }
                double chg = dPay-Double.parseDouble(txtTotal.getText());
                if(chg!=0) JOptionPane.showMessageDialog(defaultpane, "Please change: "+String.valueOf(chg));
                payRec = dPay;
                printBill();
            }
        };
        btnPrintSave.setAction(cc);
        btnPrintSave.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F9,0),btnPrintSave.getActionCommand());
        btnPrintSave.getActionMap().put(btnPrintSave.getActionCommand(), cc);
        btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveBill(true);
            }
        });
        txtDiscount.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {}
            @Override
            public void focusLost(FocusEvent e) {
                calculate();
            }
        });
        txtDiscount.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {}
            @Override
            public void keyPressed(KeyEvent e) {}
            @Override
            public void keyReleased(KeyEvent e) {
                calculate();
            }
        });
        
        //set things in place
        JPanel header = new JPanel(new GridLayout(1,3,4,4));
        header.add(lblCaption);
        header.add(new JLabel(""));
        header.add(btnReturnMain);
        
        JPanel customer = new JPanel(new GridLayout(1,2,4,4));
        JPanel subCustomer = new JPanel(new SpringLayout());
        subCustomer.add(lblCustName);
        lblCustName.setLabelFor(txtCustName);
        subCustomer.add(txtCustName);
        subCustomer.add(lblCustAddr);
        lblCustAddr.setLabelFor(txtCustAddr);
        subCustomer.add(txtCustAddr);
        customer.add(subCustomer);
        SpringUtilities.makeCompactGrid(subCustomer, 2, 2, 4, 4, 4, 4);
        customer.add(new JLabel(""));
        
        JPanel body = new JPanel(new GridLayout(1,1,4,4));
        JScrollPane subbody = new JScrollPane(tblBilling);
        body.add(subbody);
        tblBilling.setFillsViewportHeight(true);
        
        JPanel packing = null;
        packing = new JPanel(new FlowLayout());
        for(int i = 0 ; i<5 ; i++) {
            packing.add(radPacking[i]);
        }
        
        JPanel footer = new JPanel(new GridLayout(1,3,4,4));
        JPanel control = new JPanel(new GridLayout(2,2,4,4));
        control.add(new JLabel(""));
        control.add(btnSave);
        control.add(btnClear);
        control.add(btnPrintSave);
        footer.add(control);
        JPanel addrmv = new JPanel(new GridLayout(2,1,4,4));
        addrmv.add(btnAddItem);
        addrmv.add(btnRemoveItem);
        footer.add(addrmv);
        
        JPanel TOT = new JPanel(new SpringLayout());
        TOT.add(lblSubTotal);
        lblSubTotal.setLabelFor(txtSubTotal);
        TOT.add(txtSubTotal);
        TOT.add(new JLabel("Baht"));
        TOT.add(lblDiscount);
        lblDiscount.setLabelFor(txtDiscount);
        TOT.add(txtDiscount);
        TOT.add(smallB);
        TOT.add(lblTotal);
        lblTotal.setLabelFor(txtTotal);
        TOT.add(txtTotal);
        TOT.add(bigB);
        SpringUtilities.makeCompactGrid(TOT, 3, 3, 4, 4, 4, 4);
        footer.add(TOT);
        
        defaultpane.add(header);
        defaultpane.add(customer);
        defaultpane.add(body);
        defaultpane.add(packing);
        defaultpane.add(footer);
        
        pack();
        validate();
        
        initBill();
    }
    
    public void exitting() {
        Object[] options = {"Yes", "No"};
        int ch = JOptionPane.showOptionDialog(defaultpane,"Do you want to exit", "simplePOS", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        if(ch==0) {
            this.dispose();
        }
    }
    
    private void updateCustAddr() {
        int index = txtCustName.getSelectedIndex();
        txtCustAddr.setText(mainUI.myCustomer.get(index).getAddress());
    }
    private int findProduct(String bar) {
        for(int i = 0; i<mainUI.myProduct.size();i++) {
            if(bar.equals(mainUI.myProduct.get(i).getBarcode())) {
                return i;
            }
        }
        return -1;
    }
    private void addItem() {
        System.out.println("Add Item");
        String barcode = JOptionPane.showInputDialog(defaultpane,"Enter Barcode: ","simplePOS",JOptionPane.QUESTION_MESSAGE);
        int prodID;
        if(barcode.equals("")) {
            prodID = -1;
        } else {
            prodID = findProduct(barcode);
        }
        if(prodID!=-1) {
            Product selected = mainUI.myProduct.get(prodID);
            int qty = Integer.parseInt( (String) JOptionPane.showInputDialog(defaultpane, "Enter Quantity :","simplePOS",JOptionPane.QUESTION_MESSAGE,null,null,"1"));
            double pri = Double.parseDouble( (String) JOptionPane.showInputDialog(defaultpane, "Enter Price :","simplePOS",JOptionPane.QUESTION_MESSAGE,null,null,selected.getPrice()));
            double total = (double) qty*pri;
            model.addRow(new Object[] {model.getRowCount()+1,selected.getProdName(),qty,pri,total});
            currentBill.add(new BillSub(selected.getProdName(),qty,pri));
            tblBilling.setRowSelectionInterval(model.getRowCount()-1, model.getRowCount()-1);
            calculate();
        } else {
            // no tengo en la database
        }
    }
    private void removeItem() {
        System.out.println("remove Item");
        Object[] options = {"Yes", "No"};
        int ch = JOptionPane.showOptionDialog(defaultpane,"Do you want to remove selected entry?", "simplePOS", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
        if(ch==0) {
            int sel = tblBilling.getSelectedRow();
            model.removeRow(sel);
            currentBill.remove(sel);
            calculate();
        }
    }
    private void calculate() {
        double subtot,tot;
        subtot = 0;
        for(BillSub bs: currentBill) {
            subtot += bs.getTotal();
        }
        tot = subtot - Double.parseDouble(txtDiscount.getText());
        txtSubTotal.setText(String.valueOf(subtot));
        txtTotal.setText(String.valueOf(tot));
    }
    private void initBill() {
        currentBill = new ArrayList<>();
        model.setRowCount(0);
        txtCustName.setSelectedIndex(0);
    }
    private void saveBill(boolean ask) {
        //some code to save
        System.out.println("Saving");
        System.out.println(txtCustName.getSelectedItem().toString());
        mainUI.myBill.add(new Bill(txtCustName.getSelectedItem().toString(), txtCustAddr.getText(), currentBill, Double.parseDouble(txtSubTotal.getText()), Double.parseDouble(txtDiscount.getText())));
        //end save
        if(ask) {
            Object[] options = {"Yes", "No"};
            int ch = JOptionPane.showOptionDialog(defaultpane,"Save Complete, clear this bill?", "simplePOS", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
            if(ch==0) {
                initBill();
            }
        }
    }
    private void printBill() {
        System.out.println("Print");
        if(!currentBill.isEmpty()) {
            saveBill(false);
            //print function here
            PrintWriter pw = null;
            try {
                String time = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
                Bill lastBill = mainUI.myBill.get(mainUI.myBill.size()-1);
                pw = new PrintWriter(new FileOutputStream("reciept"+time+".csv",true));
                pw.printf("Time of transaction %s\r\n", lastBill.getTimestamp());
                pw.printf("Customer: %s\r\n", lastBill.getCustName());
                pw.printf("Address : %s\r\n\r\n", lastBill.getCustAddr());
                pw.printf("ID,Product Name,Quantity,Price,Total\r\n");
                for(int i=0; i<lastBill.getItem().size(); i++) {
                    pw.printf("%d,%s,%d,%.2f,%.2f\r\n", i+1,lastBill.getItem().get(i).getProdName(),lastBill.getItem().get(i).getQty(),lastBill.getItem().get(i).getPrice(),lastBill.getItem().get(i).getTotal());
                }
                pw.printf("\r\n Subtotal, %.2f\r\n", lastBill.getSubTotal());
                pw.printf("Discount, %.2f\r\n", lastBill.getDiscount());
                pw.printf("Total, %.2f\r\n\r\n\r\n", lastBill.getTotal());
                pw.printf("Packing: %s", packMode);
            } catch (Exception e) {
                System.out.printf("Something went wrong in writing current transaction");
            }
            pw.close();
            
            PrintService ps = findPrintService("CITIZEN CT-S310II");
            PrinterJob pj = PrinterJob.getPrinterJob();
            PageFormat pf = pj.defaultPage();
            Paper p = pf.getPaper();
            p.setSize(3.14961 * 72, 5 * 72);
            p.setImageableArea(0.1 * 72, 0.1 * 72, 2.94961 * 72, 5 * 72);
            pf.setPaper(p);
            try {
                pj.setPrintService(ps);
                Book b = new Book();
                b.append(this, pf);
                pj.setPageable(b);
                pj.print();
            } catch(Exception e) {
                e.printStackTrace();
            }
            initBill();
        }
    }
    
    
    //PRINTER ------------------------------------------------------------------
    
    public PrintService findPrintService(String printerName) {
        for (PrintService service : PrinterJob.lookupPrintServices()) {
            if (service.getName().equalsIgnoreCase(printerName)) return service;
        }
        return null;
    }
    
    @Override
    public int print(Graphics g, PageFormat pf, int page) throws PrinterException {

        if (page > 0) { /* We have only one page, and 'page' is zero-based */
            return NO_SUCH_PAGE;
        }

        /* User (0,0) is typically outside the imageable area, so we must
         * translate by the X and Y values in the PageFormat to avoid clipping
         */
        Graphics2D g2d = (Graphics2D)g;
        g2d.translate(pf.getImageableX(), pf.getImageableY());
        /*pf.setOrientation(PageFormat.PORTRAIT);
        Paper pPaper = pf.getPaper();
        pPaper.setImageableArea(1.0, 1.0, pPaper.getWidth() , pPaper.getHeight() -2);
        pf.setPaper(pPaper);*/

        /* Now we perform our rendering */
        g.setFont(new Font("Microsoft Sans Serif",0,12));
        FontMetrics fontMetrics = g.getFontMetrics();
        int rightEdge = 207;
        //g.drawString("str", rightEdge - fontMetrics.stringWidth("str"), y_location); //draw right align
        //g.drawString("LongerStr", (rightEdge - fontMetrics.stringWidth("LongerStr"))/2, 75); //draw center align
        
        //Bill Header
        String cName = mainUI.myCompany.getCompanyName();
        String cA1   = mainUI.myCompany.getAddress1();
        String cA2   = mainUI.myCompany.getAddress2();
        String cA3   = mainUI.myCompany.getAddress3();
        String cT1   = mainUI.myCompany.getTelephone1();
        String cT2   = mainUI.myCompany.getTelephone2();
        String cT    = "Tel: "+cT1+" Tel: "+cT2;
        g.drawString(cName, (rightEdge - fontMetrics.stringWidth(cName))/2, 15);
        g.drawString(cA1, (rightEdge - fontMetrics.stringWidth(cA1))/2, 30);
        g.drawString(cA2, (rightEdge - fontMetrics.stringWidth(cA2))/2, 45);
        g.drawString(cA3, (rightEdge - fontMetrics.stringWidth(cA3))/2, 60);
        g.drawString(cT, (rightEdge - fontMetrics.stringWidth(cT))/2, 75);
        
        //Bill Element
        int currY = 105;
        for(BillSub b : currentBill) {
            String str1 = b.getProdName();
            String str2 = b.getQty()+"@"+b.getPrice()+"= "+b.getTotal()+"฿";
            g.drawString(str1, 5, currY);
            g.drawString(str2, rightEdge - fontMetrics.stringWidth(str2), currY+15);
            currY += 30;
        }
        
        //Bill Summary
        currY += 15;
        String[] s = new String[5];
        boolean hasDis=false;
        if(Double.parseDouble(txtDiscount.getText())!=0) {
            hasDis=true;
            s[0] = "Sub-Total: "+txtSubTotal.getText()+"฿";
            s[1] = "Discount: "+Double.parseDouble(txtDiscount.getText())+"฿";
            s[2] = "Payment Due: "+txtTotal.getText()+"฿";
            s[3] = "Payment Recieved: "+String.valueOf(payRec)+"฿";
            double chg = payRec-Double.parseDouble(txtTotal.getText());
            s[4] = "Change: "+chg+"฿";
        } else {
            s[0] = "Payment Due: "+txtTotal.getText()+"฿";
            s[1] = "Payment Recieved: "+String.valueOf(payRec)+"฿";
            double chg = payRec-Double.parseDouble(txtTotal.getText());
            s[2] = "Change: "+chg+"฿";
        }
        String ty = "Thank you for shopping with us!";
        for(int i = 0 ; i<3 ; i++) {
            g.drawString(s[i], rightEdge - fontMetrics.stringWidth(s[i]), currY);
            currY += 15;
        }
        if(hasDis) {
            g.drawString(s[3], rightEdge - fontMetrics.stringWidth(s[4]), currY);
            currY += 15;
            g.drawString(s[4], rightEdge - fontMetrics.stringWidth(s[4]), currY);
            currY += 15;
        }
        g.drawString(ty, (rightEdge - fontMetrics.stringWidth(ty))/2, currY);

        /* tell the caller that this page is part of the printed document */
        return PAGE_EXISTS;
    }
}
