/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simplepos;

import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author VerifyFX
 */
public class Bill {
    private String custName;
    private String custAddr;
    private ArrayList<BillSub> item;
    private double subTotal, discount, Total;
    private String timestamp;

    public Bill(String custName, String custAddr, ArrayList<BillSub> item, double subTotal, double discount) {
        this.custName = custName;
        this.custAddr = custAddr;
        this.item = item;
        this.subTotal = subTotal;
        this.discount = discount;
        this.Total = subTotal-discount;
        this.timestamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustAddr() {
        return custAddr;
    }

    public void setCustAddr(String custAddr) {
        this.custAddr = custAddr;
    }

    public ArrayList<BillSub> getItem() {
        return item;
    }

    public void setItem(ArrayList<BillSub> item) {
        this.item = item;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTotal() {
        return Total;
    }

    public String getTimestamp() {
        return timestamp;
    }
    
}
